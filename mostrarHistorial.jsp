<%-- 
    Document   : mostrarHistorial
    Created on : 5/06/2020, 08:22:22 PM
    Author     : TereDuran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"
        
import ="java.sql.Connection"        
import ="java.sql.DriverManager"        
import ="java.sql.ResultSet"        
import ="java.sql.Statement"        
import ="java.sql.SQLException"

%>

<%
    Class.forName("com.mysql.cj.jdbc.Driver");
    Connection conex = (Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1/jugueteeducativo?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true&useSSL=false","root","Familia.dl8");
    Statement sql = conex.createStatement();
    String qry = "select * from juguete where visible=1";
    ResultSet data = sql.executeQuery(qry);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Historial</title>
    </head>
    <body bgcolor="Lightsteelblue">
        <div align = "center">
        <h1>Historial de Juguetes Educativos registrados</h1>
        <FONT SIZE=4><table width="100%" border="2" align="left" " bgcolor="Slateblue">
            
            <tr>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Precio</th>
                <th>Calificación</th>
                <th>Fecha</th>
                <th>Opción para editar</th>
                <th>Opción para eliminar</th>
            </tr>
            <%
                while (data.next()){
            %>
            <tr>
                <td><%= data.getString("nombre")%></td>
                <td><%= data.getString("descripcion")%></td>
                <td><%= data.getString("precio")%></td>
                <td><%= data.getString("calificacion")%></td>
                <td><%= data.getString("fecha")%></td>
                <td><a href="editar.jsp?id=<%=data.getString("id")%>">Editar</a></td>
                <td><a href="Eliminar.jsp?id=<%=data.getInt("id")%>">Eliminar</a></td>
            </tr>
            <%
                }
                data.close();
            %>
        </table><br><br>
        <br><a style="font-size:20px;" href="index.html">Regresar a la página principal</a>
        </div>
    </body>
</html>